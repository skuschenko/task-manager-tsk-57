package com.tsc.skuschenko.tm.listener;

import com.tsc.skuschenko.tm.api.ILoggingService;
import com.tsc.skuschenko.tm.service.LoggingService;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@NoArgsConstructor
@Service
public class LoggerListener implements MessageListener {

    @NotNull
    final ILoggingService loggingService = new LoggingService();

    @Override
    public void onMessage(@NotNull final Message message) {
        if (message instanceof TextMessage) {
            loggingService.writeLog((TextMessage) message);
        }
    }

}
