package com.tsc.skuschenko.tm.repository.dto;

import com.tsc.skuschenko.tm.api.repository.dto.ISessionDTORepository;
import com.tsc.skuschenko.tm.dto.SessionDTO;
import lombok.NoArgsConstructor;
import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class SessionDTORepository
        extends AbstractDTORepository<SessionDTO>
        implements ISessionDTORepository {

    public SessionDTORepository(
            @NotNull @Autowired EntityManager entityManager
    ) {
        this.entityManager = entityManager;
    }

    @Override
    public @Nullable List<SessionDTO> findAll(@NotNull String userId) {
        @NotNull final String query =
                "SELECT e FROM SessionDTO e WHERE e.userId = :userId";
        return entityManager.createQuery(query, SessionDTO.class)
                .setParameter("userId", userId)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Override
    public SessionDTO findSessionById(SessionDTO session) {
        return entityManager.find(SessionDTO.class, session.getId());
    }

    @Override
    public void removeSessionById(SessionDTO session) {
        @NotNull final String query =
                "DELETE FROM SessionDTO e WHERE e.id = :id";
        entityManager.createQuery(query)
                .setParameter("id", session.getId())
                .executeUpdate();
    }

}
