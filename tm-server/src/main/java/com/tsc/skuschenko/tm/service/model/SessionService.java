package com.tsc.skuschenko.tm.service.model;

import com.tsc.skuschenko.tm.api.repository.model.ISessionRepository;
import com.tsc.skuschenko.tm.api.service.IPropertyService;
import com.tsc.skuschenko.tm.api.service.model.ISessionService;
import com.tsc.skuschenko.tm.api.service.model.IUserService;
import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import com.tsc.skuschenko.tm.model.Session;
import com.tsc.skuschenko.tm.model.User;
import com.tsc.skuschenko.tm.repository.model.SessionRepository;
import com.tsc.skuschenko.tm.util.HashUtil;
import com.tsc.skuschenko.tm.util.SignatureUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.Optional;

@Getter
@Service
@NoArgsConstructor
public class SessionService extends AbstractBusinessService<Session>
        implements ISessionService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private IUserService userService;

    @Override
    @Nullable
    public User checkDataAccess(
            @Nullable final String login,
            @Nullable final String password) {
        if (!Optional.ofNullable(login).isPresent()) return null;
        if (!Optional.ofNullable(password).isPresent()) return null;
        @Nullable final User user = userService.findByLogin(login);
        if (!Optional.ofNullable(user).isPresent()) return null;
        @Nullable final String secret = propertyService.getPasswordSecret();
        @Nullable final Integer iteration =
                propertyService.getPasswordIteration();
        @Nullable final String passwordHash =
                HashUtil.salt(secret, iteration, password);
        if (!Optional.ofNullable(passwordHash).isPresent() ||
                !passwordHash.equals(user.getPasswordHash())) return null;
        return user;
    }

    @Override
    @SneakyThrows
    public void close(@NotNull final Session session)
            throws AccessForbiddenException {
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final ISessionRepository sessionRepository =
                context.getBean(SessionRepository.class, entityManager);
        try {
            entityManager.getTransaction().begin();
            @Nullable final Session sessionFind =
                    sessionRepository.getReference(session.getId());
            Optional.ofNullable(sessionFind)
                    .ifPresent(sessionRepository::remove);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session open(
            @Nullable final String login, @Nullable final String password
    ) {
        @Nullable final User user = checkDataAccess(login, password);
        Optional.ofNullable(user).orElseThrow(AccessForbiddenException::new);
        @NotNull final Session session = new Session();
        session.setUser(user);
        session.setTimestamp(System.currentTimeMillis());
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final ISessionRepository sessionRepository =
                context.getBean(SessionRepository.class, entityManager);
        try {
            entityManager.getTransaction().begin();
            sessionRepository.add(session);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return sign(session);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session sign(@Nullable final Session session) {
        if (!Optional.ofNullable(session).isPresent()) return null;
        session.setSignature(null);
        @Nullable final String salt = propertyService.getSessionSalt();
        if (!Optional.ofNullable(salt).isPresent()) return null;
        @Nullable final String cycle = propertyService.getSessionCycle();
        if (!Optional.ofNullable(cycle).isPresent()) return null;
        final int iteration = Integer.parseInt(cycle);
        @Nullable final String signature =
                SignatureUtil.sign(session, salt, iteration);
        session.setSignature(signature);
        return session;
    }

    @Override
    public void validate(@Nullable Session session, @Nullable Role role)
            throws AccessForbiddenException {
        Optional.ofNullable(role)
                .orElseThrow(AccessForbiddenException::new);
        validate(session);
        @Nullable final String userId = session.getUser().getId();
        Optional.ofNullable(userId)
                .orElseThrow(AccessForbiddenException::new);
        @Nullable final User user = userService.findById(userId);
        Optional.ofNullable(user)
                .orElseThrow(AccessForbiddenException::new);
        Optional.ofNullable(user.getRole())
                .orElseThrow(AccessForbiddenException::new);
        if (!role.getDisplayName().equals(user.getRole()))
            throw new AccessForbiddenException();
    }

    @Override
    public void validate(@Nullable final Session session)
            throws AccessForbiddenException {
        Optional.ofNullable(session)
                .orElseThrow(AccessForbiddenException::new);
        Optional.ofNullable(session.getSignature())
                .orElseThrow(AccessForbiddenException::new);
        Optional.ofNullable(session.getUser())
                .orElseThrow(AccessForbiddenException::new);
        Optional.ofNullable(session.getTimestamp())
                .orElseThrow(AccessForbiddenException::new);
        @Nullable final Session tempSession = session.clone();
        Optional.ofNullable(tempSession)
                .orElseThrow(AccessForbiddenException::new);
        @NotNull final String signatureSource = session.getSignature();
        @Nullable final Session signSession = sign(tempSession);
        Optional.ofNullable(signSession)
                .orElseThrow(AccessForbiddenException::new);
        @Nullable final String signatureTarget = signSession.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessForbiddenException();
    }

}
