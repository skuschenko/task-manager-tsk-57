package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.service.IPropertyService;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Properties;

@Service
public class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "version";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "author.email";

    @NotNull
    private static final String AUTHOR_KEY = "author";

    @NotNull
    private static final String BACKUP_DEFAULT_TIME = "60";

    @NotNull
    private static final String BACKUP_TIME = "backupTime";

    @NotNull
    private static final String FACTORY_CLASS = "jdbc.cache.factoryClass";

    @NotNull
    private static final String FILE_BACKUP_XML = "filePath.backupXml";

    @NotNull
    private static final String FILE_BASE64 = "filePath.base64";

    @NotNull
    private static final String FILE_BINARY = "filePath.binary";

    @NotNull
    private static final String FILE_JAXB_JSON = "filePath.jaxbJson";

    @NotNull
    private static final String FILE_JAXB_XML = "filePath.jaxbXml";

    @NotNull
    private static final String FILE_JSON = "filePath.json";

    @NotNull
    private static final String FILE_SCANNER = "fileScannerTime";

    @NotNull
    private static final String FILE_SCANNER_TIME = "60";

    @NotNull
    private static final String FILE_XML = "filePath.xml";

    @NotNull
    private static final String FILE_YAML = "filePath.yaml";

    @NotNull
    private static final String JDBC_DIALECT = "jdbc.dialect";

    @NotNull
    private static final String JDBC_DRIVER = "jdbc.driver";

    @NotNull
    private static final String JDBC_HBM2DDL = "jdbc.hbm2ddl";

    @NotNull
    private static final String JDBC_PASSWORD = "jdbc.password";

    @NotNull
    private static final String JDBC_SHOW_SQL = "jdbc.showSql";

    @NotNull
    private static final String JDBC_URL = "jdbc.url";

    @NotNull
    private static final String JDBC_USER_NAME = "jdbc.userName";

    @NotNull
    private static final String LITE_MEMBER = "jdbc.cache.liteMember";

    @NotNull
    private static final String MINIMAL_PUTS = "jdbc.cache.minimalPuts";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "25456";

    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "356585985";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String PROVIDER_CONFIGURATION =
            "jdbc.cache.providerConfiguration";

    @NotNull
    private static final String QUERY_CACHE = "jdbc.cache.queryCache";

    @NotNull
    private static final String REGION_PREFIX = "jdbc.cache.regionPrefix";

    @NotNull
    private static final String SECOND_LEVEL = "jdbc.cache.secondLevel";

    @NotNull
    private static final String SERVER_HOST = "server.host";

    @NotNull
    private static final String SERVER_PORT = "server.port";

    @NotNull
    private static final String SESSION_CYCLE = "session.cycle";

    @NotNull
    private static final String SESSION_SALT = "session.salt";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        if (System.getProperties().contains(APPLICATION_VERSION_KEY)) {
            return System.getProperty(APPLICATION_VERSION_KEY);
        }
        if (System.getenv().containsKey(APPLICATION_VERSION_KEY)) {
            return System.getenv(APPLICATION_VERSION_KEY);
        }
        return properties.getProperty(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthor() {
        if (System.getProperties().contains(AUTHOR_KEY)) {
            return System.getProperty(AUTHOR_KEY);
        }
        if (System.getenv().containsKey(AUTHOR_KEY)) {
            return System.getenv(AUTHOR_KEY);
        }
        return properties.getProperty(AUTHOR_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        if (System.getProperties().contains(AUTHOR_EMAIL_KEY)) {
            return System.getProperty(AUTHOR_EMAIL_KEY);
        }
        if (System.getenv().containsKey(AUTHOR_EMAIL_KEY)) {
            return System.getenv(AUTHOR_EMAIL_KEY);
        }
        return properties.getProperty(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public Integer getBackupTime() {
        return Integer.parseInt(
                Optional.ofNullable(properties.getProperty(BACKUP_TIME))
                        .orElse(BACKUP_DEFAULT_TIME)
        );
    }

    @Nullable
    @Override
    public String getFactoryClass() {
        return properties.getProperty(FACTORY_CLASS);
    }

    @Override
    public @Nullable String getFileBackupPath() {
        return properties.getProperty(FILE_BACKUP_XML);
    }

    @Nullable
    @Override
    public String getFileBase64Path() {
        return properties.getProperty(FILE_BASE64);
    }

    @Nullable
    @Override
    public String getFileBinaryPath() {
        return properties.getProperty(FILE_BINARY);
    }

    @Nullable
    @Override
    public String getFileJsonPath(@Nullable final String className) {
        switch (className) {
            case "jaxB":
                return properties.getProperty(FILE_JAXB_JSON);
            default:
                return properties.getProperty(FILE_JSON);
        }
    }

    @Override
    public @NotNull Integer getFileScannerTime() {
        return Integer.parseInt(
                Optional.ofNullable(properties.getProperty(FILE_SCANNER))
                        .orElse(FILE_SCANNER_TIME)
        );
    }

    @Nullable
    @Override
    public String getFileXmlPath(@Nullable final String className) {
        switch (className) {
            case "jaxb":
                return properties.getProperty(FILE_JAXB_XML);
            default:
                return properties.getProperty(FILE_XML);
        }
    }

    @Nullable
    @Override
    public String getFileYamlPath() {
        return properties.getProperty(FILE_YAML);
    }

    @Nullable
    @Override
    public String getJdbcDialect() {
        return properties.getProperty(JDBC_DIALECT);
    }

    @Nullable
    @Override
    public String getJdbcDriver() {
        if (System.getenv().containsKey(JDBC_DRIVER)) {
            return System.getenv(JDBC_DRIVER);
        }
        return properties.getProperty(JDBC_DRIVER);
    }

    @Nullable
    @Override
    public String getJdbcHbm2ddl() {
        return properties.getProperty(JDBC_HBM2DDL);
    }

    @Nullable
    @Override
    public String getJdbcPassword() {
        if (System.getenv().containsKey(JDBC_PASSWORD)) {
            return System.getenv(JDBC_PASSWORD);
        }
        return properties.getProperty(JDBC_PASSWORD);
    }

    @Nullable
    @Override
    public String getJdbcShowSql() {
        return properties.getProperty(JDBC_SHOW_SQL);
    }

    @Nullable
    @Override
    public String getJdbcUrl() {
        if (System.getenv().containsKey(JDBC_URL)) {
            return System.getenv(JDBC_URL);
        }
        return properties.getProperty(JDBC_URL);
    }

    @Nullable
    @Override
    public String getJdbcUserName() {
        if (System.getenv().containsKey(JDBC_USER_NAME)) {
            return System.getenv(JDBC_USER_NAME);
        }
        return properties.getProperty(JDBC_USER_NAME);
    }

    @Nullable
    @Override
    public String getLiteMember() {
        return properties.getProperty(LITE_MEMBER);
    }

    @Nullable
    @Override
    public String getMinimalPuts() {
        return properties.getProperty(MINIMAL_PUTS);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        if (System.getProperties().contains(PASSWORD_ITERATION_KEY)) {
            @NotNull final String value =
                    System.getProperty(PASSWORD_ITERATION_KEY);
            return Integer.parseInt(value);
        }
        if (System.getenv().containsKey(PASSWORD_ITERATION_KEY)) {
            @NotNull final String value =
                    System.getProperty(PASSWORD_ITERATION_KEY);
            return Integer.parseInt(value);
        }
        @NotNull final String value = properties.getProperty(
                PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT
        );
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        if (System.getProperties().contains(PASSWORD_SECRET_KEY)) {
            return System.getProperty(PASSWORD_SECRET_KEY);
        }
        if (System.getenv().containsKey(PASSWORD_SECRET_KEY)) {
            return System.getenv(PASSWORD_SECRET_KEY);
        }
        return properties.getProperty(
                PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT
        );
    }

    @Nullable
    @Override
    public String getProviderConfiguration() {
        return properties.getProperty(PROVIDER_CONFIGURATION);
    }

    @Nullable
    @Override
    public String getQueryCache() {
        return properties.getProperty(QUERY_CACHE);
    }

    @Nullable
    @Override
    public String getRegionPrefix() {
        return properties.getProperty(REGION_PREFIX);
    }

    @Nullable
    @Override
    public String getSecondLevel() {
        return properties.getProperty(SECOND_LEVEL);
    }

    @Nullable
    @Override
    public String getServerHost() {
        if (System.getenv().containsKey(SERVER_HOST)) {
            return System.getenv(SERVER_HOST);
        }
        return properties.getProperty(SERVER_HOST);
    }

    @Nullable
    @Override
    public String getServerPort() {
        if (System.getenv().containsKey(SERVER_PORT)) {
            return System.getenv(SERVER_PORT);
        }
        return properties.getProperty(SERVER_PORT);
    }

    @Nullable
    @Override
    public String getSessionCycle() {
        return properties.getProperty(SESSION_CYCLE);
    }

    @Nullable
    @Override
    public String getSessionSalt() {
        return properties.getProperty(SESSION_SALT);
    }

}
