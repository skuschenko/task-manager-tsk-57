package com.tsc.skuschenko.tm.repository.dto;

import com.tsc.skuschenko.tm.api.repository.dto.IAbstractDTORepository;
import com.tsc.skuschenko.tm.dto.AbstractEntityDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public abstract class AbstractDTORepository<E extends AbstractEntityDTO>
        implements IAbstractDTORepository<E> {

    @NotNull
    protected EntityManager entityManager;

    @Override
    public void add(@NotNull final E entity) {
        System.out.println(entityManager.hashCode());
        entityManager.persist(entity);
    }

    @Override
    @Nullable
    public E getEntity(@NotNull TypedQuery<E> query) {
        @NotNull final List<E> projectDTOList = query.getResultList();
        if (projectDTOList.isEmpty()) {
            return null;
        }
        return projectDTOList.get(0);
    }

    @Override
    public void remove(@NotNull final E entity) {
        entityManager.remove(entity);
    }

    @Override
    public void update(@NotNull final E entity) {
        entityManager.merge(entity);
    }

}
