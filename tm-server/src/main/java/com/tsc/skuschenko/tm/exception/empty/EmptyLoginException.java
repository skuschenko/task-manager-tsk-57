package com.tsc.skuschenko.tm.exception.empty;

import com.tsc.skuschenko.tm.exception.AbstractException;

public final class EmptyLoginException extends AbstractException {

    public EmptyLoginException() {
        super("Error! LOGIN is empty...");
    }

}
