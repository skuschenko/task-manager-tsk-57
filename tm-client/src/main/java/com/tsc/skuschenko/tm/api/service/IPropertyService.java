package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.api.other.ISaltSetting;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IPropertyService extends ISaltSetting {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthor();

    @NotNull
    String getAuthorEmail();

    @NotNull
    Integer getBackupTime();

    @Nullable
    String getFileBackupPath();

    @Nullable
    String getFileBase64Path();

    @Nullable
    String getFileBinaryPath();

    @Nullable
    String getFileJsonPath(@Nullable String className);

    @NotNull
    Integer getFileScannerTime();

    @Nullable
    String getFileXmlPath(@Nullable String className);

    @Nullable
    String getFileYamlPath();

    @Nullable
    String getServerHost();

    @Nullable
    String getServerPort();

    @Nullable
    String getSessionCycle();

    @Nullable
    String getSessionSalt();

}
