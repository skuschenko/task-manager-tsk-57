package com.tsc.skuschenko.tm.listener.user;

import com.tsc.skuschenko.tm.api.service.ISessionService;
import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.endpoint.User;
import com.tsc.skuschenko.tm.endpoint.UserEndpoint;
import com.tsc.skuschenko.tm.event.ConsoleEvent;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public final class UserRegistryListener extends AbstractUserListener {

    private static final String DESCRIPTION = "registry user";

    private static final String NAME = "registry-user";

    @Autowired
    private ISessionService sessionService;

    @Autowired
    private UserEndpoint userEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@userRegistryListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        showOperationInfo(NAME);
        showParameterInfo("login");
        @Nullable final Session session = sessionService.getSession();
        Optional.ofNullable(session).orElseThrow(AccessForbiddenException::new);
        @NotNull final String login = TerminalUtil.nextLine();
        showParameterInfo("password");
        @NotNull final String password = TerminalUtil.nextLine();
        showParameterInfo("email");
        @NotNull final String email = TerminalUtil.nextLine();
        @NotNull final User user = userEndpoint
                .createUserWithEmail(login, password, email);
        showUser(user);
    }

    @Override
    public String name() {
        return NAME;
    }

}
