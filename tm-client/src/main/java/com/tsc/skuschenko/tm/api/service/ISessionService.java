package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.endpoint.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface ISessionService {

    @Nullable Session getSession();

    void setSession(@NotNull Session session);

}
